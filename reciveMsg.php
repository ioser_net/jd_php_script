<?php
require_once './evaluate.php';

use think\facade\Db;

Db::setConfig([
    // 默认数据连接标识
    'default'     => 'sqlite',
    // 数据库连接信息
    'connections' => [
        'sqlite' => [
            'type'        => 'sqlite',
            'database'    => './jd.sqlite3',
            'trigger_sql' => true
        ],
    ],
]);


use EasyWeChat\Factory;

if (empty(WX_CORPID) || empty(WX_AGENT_ID) || empty(WX_CORPSECRET) || empty(WX_SERVER_TOKEN) || empty(WX_SERVER_AES_KEY)) {
    echo "请检查配置文件是否填写正确";
    die;
}
$workWechatConfig = [
    'corp_id'       => WX_CORPID,
    'agent_id'      => WX_AGENT_ID,
    'secret'        => WX_CORPSECRET,
    'token'         => WX_SERVER_TOKEN,
    'aes_key'       => WX_SERVER_AES_KEY,
    'response_type' => 'array',
    'log'           => [
        'level' => 'debug',
        'file'  => __DIR__ . '/wechat.log',
    ],
];
$app              = Factory::work($workWechatConfig);
//
$app->server->push(function ($msg) use ($app) {
    return handlerMsg($msg, COMMANDS, MSG_TIP_TEXT, $app);
});
$response = $app->server->serve();
$response->send();
/**
 * handlerMsg 消息处理
 * @param $msg
 * @param $commands
 * @param $msgTipText
 * @param $app
 * 2021/9/10 10:32 上午
 * @return false|mixed|string
 * @author 田继业 <tjy_we@163.com>
 */
function handlerMsg($msg, $commands, $msgTipText, $app)
{
    // 判断是否首次运行服务端 添加菜单
    if (!file_exists('./init_server_flag')) {
        file_put_contents('./init_server_flag', 'ok');
        $resp = objToArray(json_decode(updateMenus($app)));
        if ($resp['errcode'] == 0) {
            return '首次运行更新菜单成功';
        } else {
            return json_encode($resp);
        }
    }
    $userId  = $msg['FromUserName'];
    $msgJson = date('Y-m-d H:i:s') . ": $userId" . PHP_EOL . json_encode($msg, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . PHP_EOL . PHP_EOL;
    file_put_contents('./msg.log', $msgJson, FILE_APPEND);
    if (!empty(WX_DEBUG) && WX_DEBUG === true && YOU_WX_USER_ID !== $userId) {
        sendHelpWxNotify(YOU_WX_USER_ID, $msgJson, $userId);
    }
    $resultMsg = '';
    switch ($msg["MsgType"]) {
        case 'event':
            if ($msg['EventKey'] == '登录') {
                $resultMsg .= '请输入手机号';
                Db::name('jd_msg_history')->insert(['userid' => $userId, 'command' => '登录']);
            } elseif ($msg['EventKey'] == '资产变动通知') {
                $resultMsg = sendBeanChangeNotify($userId);
            } else if ($msg['EventKey'] == '资产变动通知强化版') {
                $resultMsg = sendALlBeanChangeNotify($userId);
            } else if ($msg['EventKey'] == '京豆签到') {
                $resultMsg = beanSign($userId);
            } else if ($msg['EventKey'] == '城城分现金') {
//                $resultMsg = '维护中';
                $resultMsg = cityCash($userId);
            } else if ($msg['EventKey'] == '指令帮助') {
                sendHelpWxNotify($userId, commandHelp($commands));
                $resultMsg = '';
            } else if ($msg['EventKey'] == '查询绑定京东账户') {
                $users     = objToArray(json_decode(file_get_contents('./JDAccount.json')));
                $resultMsg = "查询绑定京东账户\n用户:$userId\n\n目前已绑定京东账户:\n " . implode("\n ", $users[$userId]);
                $resultMsg .= $msgTipText;
            } else if ($msg['EventKey'] == '自动评价') {
                //锁定用户 不让这个用户继续执行

                $cookies = getCookies($userId);
                if ($cookies) {
                    $currentUser = objToArray(json_decode(file_get_contents('EvalCurrentUser.json')));
                    if (!empty($currentUser) && $currentUser[$userId]) {
                        return "自动评价\n当前任务正在进行，请稍后";
                    }
                    $currentUser[$userId] = true;
                    file_put_contents('EvalCurrentUser.json', json_encode($currentUser));
                    $users     = objToArray(json_decode(file_get_contents('./JDAccount.json')));
                    $resultMsg = "自动评价\n获取绑定用户列表：\n\t" . implode("\n\t", $users[$userId]);
                    $logPath   = '/jd/log/jd_evaluate/';
                    if (!is_dir($logPath)) {
                        mkdir($logPath);;
                    }
                    $logFile = $logPath . date('Y-m-d-H-i-s') . '-' . $userId . '.log';
                    exec("php -r \"require '/usr/share/nginx/html/evaluate.php';start('" . $userId . "','" . implode(',', $cookies) . "');\" > " . $logFile . " &");
                    return $resultMsg;
                } else {
                    $resultMsg = "京东资产变动通知：\n未找到用户：$userId";
                    return $resultMsg;
                }

            } else if ($msg['EventKey'] == 'subscribe') {
                $resultMsg = "欢迎进入薅羊毛小分队！！！";
                $resultMsg .= $msgTipText;
            } else if ($msg['EventKey'] == '价格保护') {
                $resultMsg = priceProtection($userId);

            }
            break;
        case 'text':
            if ($msg["Content"] == "更新菜单") {
                $resultMsg = updateMenus($app);
//            return json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)
            } else if ($msg['Content'] == '资产变动通知') {
                $resultMsg = sendBeanChangeNotify($userId);
            } else if ($msg['Content'] == '资产变动通知强化版') {
                $resultMsg = sendALlBeanChangeNotify($userId);
            } else if (!empty($commands[$msg["Content"]])) {
                $resultMsg = runOtherCommand($msg["Content"], $commands[$msg["Content"]], $userId);
            } else if ($msg['Content'] == "查询绑定京东账户" || $msg['Content'] == "查询绑定京东账号") {
                $users     = objToArray(json_decode(file_get_contents('./JDAccount.json')));
                $resultMsg = "查询绑定京东账户\n用户:$userId\n\n目前已绑定京东账户:\n " . implode("\n ", $users[$userId]);
                $resultMsg .= "\n\n使用帮助：\n";
                $resultMsg .= $msgTipText;
            } else if (strstr($msg['Content'], '绑定京东账户') || strstr($msg['Content'], '绑定京东账号')) {
                $content = str_replace(" ", "", $msg['Content']);
                $account = mb_substr($content, 6);
                if (!empty($account)) {

                    $users            = objToArray(json_decode(file_get_contents('./JDAccount.json')));
                    $users[$userId][] = $account;
                    $users[$userId]   = array_unique($users[$userId]);
                    $users[$userId]   = array_values($users[$userId]);
                    file_put_contents('./JDAccount.json', json_encode($users, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
                    $resultMsg = "绑定京东账户\n用户:$userId\n\n目前已绑定京东账户:\n " . implode("\n ", $users[$userId]);
                } else {
                    $resultMsg = "绑定京东账户\n用户关系建立失败";
                }
            } else if (strstr($msg['Content'], '绑定太太乐') || strstr($msg['Content'], '绑定太太乐')) {
                if (file_exists('./ttlToken.json')) {
                    $ttlToken = objToArray(json_decode(file_get_contents('./ttlToken.json')));
                } else {
                    $ttlToken = [];
                }

                $content = explode(" ", $msg['Content']);
                if (count($content) === 3) {
                    $username = $content[1];
                    $password = $content[2];

                    if (empty(TTL_ADDR)) {
                        return "未设置TTL_ADDR";
                    }
                    $tokenRes = objToArray(json_decode(file_get_contents("http://" . TTL_ADDR . "/index?username=$username&password=$password")));
                    if (!empty($tokenRes['token'])) {
                        $ttlToken[$userId][$tokenRes['userId']] = $tokenRes['token'];
//                        $ttlToken[$userId]   = array_unique($ttlToken[$userId]);
//                        $ttlToken[$userId]   = array_values($ttlToken[$userId]);
//                        return json_encode($ttlToken, JSON_PRETTY_PRIN12T | JSON_UNESCAPED_UNICODE);
                        file_put_contents('./ttlToken.json', json_encode($ttlToken, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
                        $resultMsg = "绑定太太乐\n用户:$userId\n\n目前已绑定token:\n " . implode("\n", $ttlToken[$userId]);
                    } else {
                        $resultMsg = "绑定太太乐\n获取token失败";
                    }

                } else {
                    $resultMsg = "绑定太太乐\n格式错误！";
                }


            } else if ($msg['Content'] == '用户列表') {

                $users = objToArray(json_decode(file_get_contents('./JDAccount.json')));
                foreach ($users as $k => $value) {
                    foreach ($value as &$v) {
                        $v = "\t" . urldecode($v);
                    }
                    $userStr   = implode("\n", $value);
                    $resultMsg .= $k . "\n" . $userStr . "\n";
                }

            } else if (strstr($msg['Content'], '解绑京东账户') || strstr($msg['Content'], '解绑京东账号')) {
                $content = str_replace(" ", "", $msg['Content']);
                $account = mb_substr($content, 6);
                if (!empty($account)) {

                    $users = objToArray(json_decode(file_get_contents('./JDAccount.json')));
                    if (!empty($users[$userId])) {
                        foreach ($users[$userId] as $k => $v) {
                            if ($v == $account) {
                                unset($users[$userId][$k]);
                            }
                        }
                        file_put_contents('./JDAccount.json', json_encode($users, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
                        $resultMsg = "解绑京东账户\n用户:$userId\n\n已解除绑定：$account\n\n目前已绑定京东账户:\n " . implode("\n ", $users[$userId]);
                    } else {
                        $resultMsg = "解绑京东账户\n用户:$userId\n\n当前未绑定任何京东账户";
                    }
                } else {
                    $resultMsg = "绑定京东账户\n用户关系建立失败";
                }
            } else if ($msg['Content'] == '绑定') {
                $resultMsg .= $msgTipText;
            } else if ($msg['Content'] == '解锁') {
                //解锁用户 不让这个用户继续执行
                $currentUser          = objToArray(json_decode(file_get_contents('EvalCurrentUser.json')));
                $currentUser[$userId] = false;
                file_put_contents('EvalCurrentUser.json', json_encode($currentUser));
                $resultMsg = "用户:$userId,解锁成功。";
            } else if ($msg['Content'] == 'pull') {
                $resultMsg = "代码更新结果:\n" . shell_exec("cd /usr/share/nginx/html && git pull");
            } else if ($msg['Content'] == '互助码') {
                $resultMsg = getShareCode($userId);
            } else if (strstr($msg['Content'], '添加城城领现金互助码') || strstr($msg['Content'], '添加城城互助码')) {
                $code                  = explode(" ", $msg['Content'])[1];
                $shareCodes            = objToArray(json_decode(file_get_contents('./JDShareCode.json')));
                $shareCodes[$userId][] = $code;
                $shareCodes[$userId]   = array_unique($shareCodes[$userId]);
                $shareCodes[$userId]   = array_values($shareCodes[$userId]);

                file_put_contents('./JDShareCode.json', json_encode($shareCodes, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
                $resultMsg = '添加成功';
            } else if (strstr($msg['Content'], '查询城城领现金互助码') || strstr($msg['Content'], '查询城城互助码')) {

                $shareCodes = objToArray(json_decode(file_get_contents('./JDShareCode.json')));
                $result     = $shareCodes[$userId];
                $resultMsg  = "城城领现金互助码：\n";
                $resultMsg  .= implode("\n", $result);
                return $resultMsg;
            } else if (mb_strlen($msg['Content']) == 11 && getphonetype($msg['Content'])) {
                $latest_msg = Db::name('jd_msg_history')->where(['userid' => $userId])->order('id desc')->find();
                if (!empty($latest_msg) && $latest_msg['command'] == '登录') {
                    Db::name('jd_msg_history')->insert(['userid' => $userId, 'command' => '手机号', 'content' => $msg['Content']]);
                    $resultMsg .= system("bash /jd/ptkey.sh getsms $msg[Content]");
                    $resultMsg .= PHP_EOL;
                    $resultMsg .= '请输入京东验证码';
                }

            } else if (mb_strlen($msg['Content']) == 6) {
                $latest_msg = Db::name('jd_msg_history')->where(['userid' => $userId])->order('id desc')->find();
                if (!empty($latest_msg) && $latest_msg['command'] == '手机号') {
                    $new_cookie = system("bash /jd/ptkey.sh getck $latest_msg[content] $msg[Content]");

                    if (preg_match('/^pt_key=(.*?);pt_pin=(.*?);/', $new_cookie)) {
                        $pt_key              = explode(";", $new_cookie)[0];
                        $config_sh           = file_get_contents(CONFIG_PATH);
                        $cookies_content_arr = explode("####cookie####\n", $config_sh);
                        $cookies             = $cookies_content_arr[1];
                        $ck_arr              = explode("\n", $cookies);

                        $pt_pin = explode(";", $new_cookie)[1];
                        $flag   = 0;
                        foreach ($ck_arr as $v) {
                            if ($ck = explode("=\"", $v))
                                if (!empty($ck[0]) && !empty($ck[1])) {
                                    $cks[$ck[0]] = substr($ck[1], 0, -1);
                                    if ($pt_pin == explode(";", $cks[$ck[0]])[1]) {
                                        $urlencode_ptpin = urlencode(explode('=', $pt_pin)[1]);
                                        $urlencode_ck    = $pt_key . ";pt_pin=" . $urlencode_ptpin . ';';
                                        $cks[$ck[0]]     = $urlencode_ck;
                                        $flag            = 1;
                                    }
                                }
                        }
                        if ($flag == 0) {
                            $urlencode_ptpin                   = urlencode(explode('=', $pt_pin)[1]);
                            $urlencode_ck                      = $pt_key . ";pt_pin=" . $urlencode_ptpin . ';';
                            $cks['Cookie' . (count($cks) + 1)] = $urlencode_ck;
                        }
                        $content = '';
                        foreach ($cks as $k => $v) {
                            $content .= $k . '=' . '"' . $v . '"' . PHP_EOL;
                        }
                        $cookies_content_arr[1] = $content;
                        file_put_contents(CONFIG_PATH, implode("####cookie####\n", $cookies_content_arr));
                        $user_name = explode('=', $pt_pin)[1];
                        $user_name = urldecode($user_name);
                        $resultMsg = $user_name . ' cookie更新成功';
                        sendWxMsgCurrent($userId, $cookie);
                    } else {
                        $resultMsg = $cookie;
                    }

//                    $resultMsg .= PHP_EOL;
//                    $resultMsg .= '请输入京东验证码';
                }

            } else if (preg_match('/^pt_key=(.*?);pt_pin=(.*?);/', $msg['Content'])) {
                $new_cookie          = $msg['Content'];
                $pt_key              = explode(";", $new_cookie)[0];
                $config_sh           = file_get_contents(CONFIG_PATH);
                $cookies_content_arr = explode("####cookie####\n", $config_sh);
                $cookies             = $cookies_content_arr[1];
                $ck_arr              = explode("\n", $cookies);

                $pt_pin = explode(";", $new_cookie)[1];
                $flag   = 0;
                foreach ($ck_arr as $v) {
                    if ($ck = explode("=\"", $v))
                        if (!empty($ck[0]) && !empty($ck[1])) {
                            $cks[$ck[0]] = substr($ck[1], 0, -1);
                            if ($pt_pin == explode(";", $cks[$ck[0]])[1]) {
                                $urlencode_ptpin = urlencode(explode('=', $pt_pin)[1]);
                                $urlencode_ck    = $pt_key . ";pt_pin=" . $urlencode_ptpin . ';';
                                $cks[$ck[0]]     = $urlencode_ck;
                                $flag            = 1;
                            }
                        }
                }
                if ($flag == 0) {
                    $urlencode_ptpin                   = urlencode(explode('=', $pt_pin)[1]);
                    $urlencode_ck                      = $pt_key . ";pt_pin=" . $urlencode_ptpin . ';';
                    $cks['Cookie' . (count($cks) + 1)] = $urlencode_ck;
                }
                $content = '';
                foreach ($cks as $k => $v) {
                    $content .= $k . '=' . '"' . $v . '"' . PHP_EOL;
                }
                $cookies_content_arr[1] = $content;
                file_put_contents(CONFIG_PATH, implode("####cookie####\n", $cookies_content_arr));
                $user_name = explode('=', $pt_pin)[1];
                $user_name = urldecode($user_name);
//                $resultMsg = $user_name . ' cookie更新成功';
                sendWxMsgCurrent($userId, $new_cookie);


            } else {
                $resultMsg = '指令未定义';
            }
            break;
    }

    return $resultMsg;
}

function getShareCode($userid)
{

    $users  = objToArray(json_decode(file_get_contents('./JDAccount.json')));
    $string = $users[$userid][0];
    $dir    = new DirectoryIterator('/jd/log/jd_CityNew');
    foreach ($dir as $file) {
        $content = file_get_contents($file->getPathname());
        if (strpos($content, $string) !== false) {
            $matches = array();
            $handle  = @fopen($file->getPathname(), "r");
            if ($handle) {
                while (!feof($handle)) {
                    $buffer = fgets($handle);
                    if (strpos($buffer, '的城城领现金好友互助码') !== FALSE)
                        $matches[] = $buffer;
                }
                fclose($handle);
            }
            $res = implode("\n", $matches);
            sendHelpWxNotify($userid, $res, '城城分现金互助码');

            return '';
        }
    }
}

/**
 * runOtherCommand 运行其他命令
 * @param $title
 * @param $cmd
 * @param $userid
 * 2021/9/10 10:27 上午
 * @return string
 * @author 田继业 <tjy_we@163.com>
 */
function runOtherCommand($title, $cmd, $userid)
{
    $allCookie  = '';
    $replaceRes = replaceCookie($userid, $allCookie);

    if ($replaceRes) {
        $resultMsg = $title . "：\n用户：$userid \n正在执行$title...\n请稍后...\n";
        updateWorkWechatConfig($userid);
        $command = "bash /jd/jd.sh " . $cmd . " now " . TMP_CONFIG_PATH . "> /dev/null &";
        exec($command);
//        sleep(1);
//        restoreWorkWechatConfig();
//        restoreCookie($allCookie);
    } else {
        $resultMsg = "京东资产变动通知：\n未找到用户：$userid";
    }
    return $resultMsg;
}

/**
 * sendBeanChangeNotify 发送京东资产变动通知
 * @param $userid string 用户id
 * 2021/9/6 11:40 上午
 * @return string
 * @author 田继业 <tjy_we@163.com>
 */
function sendBeanChangeNotify($userid)
{
    $allCookie  = '';
    $replaceRes = replaceCookie($userid, $allCookie);

    if ($replaceRes) {
        $resultMsg = "京东资产变动通知：\n用户：$userid \n正在获取资产变动通知...\n请稍后...\n";
        updateWorkWechatConfig($userid);
        exec("bash /jd/jd.sh jd_bean_change_clean now " . TMP_CONFIG_PATH . "> /dev/null &");
//        sleep(1);
//        restoreWorkWechatConfig();
//        restoreCookie($allCookie);
    } else {
        $resultMsg = "京东资产变动通知：\n未找到用户：$userid";
    }
    return $resultMsg;
}

function cityCash($userid)
{
    $allCookie = '';


    $resultMsg  = "城城分现金：\n用户：$userid \n正在执行...\n请稍后前往京东APP查看结果...\n";
    $shareCodes = objToArray(json_decode(file_get_contents('./JDShareCode.json')));
    $result     = $shareCodes[$userid];
    if (empty($resultMsg)) {
        return "城城分现金：\n用户：$userid \n未绑定互助码...\n";
    }
    if (file_exists('./JDCurrentShareCode.json')) {
        return "城城分现金：\n其他用户正在执行";
    }

    file_put_contents('./JDCurrentShareCode.json', json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    sendHelpWxNotify('@all', '当前城城分现金' . $userid . '正在执行', '当前城城分现金' . $userid . '正在执行');
    exec("(bash /jd/jd.sh jd_CityNew now;rm -rf /usr/share/nginx/html/JDCurrentShareCode.json;bash /usr/share/nginx/html/sendNotify.sh) >> /dev/null  &");
    return $resultMsg;
}

/**
 * sendALlBeanChangeNotify 资产变动通知强化版
 * @param $userid string 用户id
 * 2021/9/6 2:36 下午
 * @return string
 * @author 田继业 <tjy_we@163.com>
 */
function sendALlBeanChangeNotify($userid)
{
    $allCookie  = '';
    $replaceRes = replaceCookie($userid, $allCookie);

    if ($replaceRes) {
        $resultMsg = "京东资产变动通知强化版\n用户：$userid \n正在获取资产变动通知...\n请稍后...\n别急。。。\n很慢。。。。。\n";
        updateWorkWechatConfig($userid);
        exec("bash /jd/jd.sh jd_bean_change now " . TMP_CONFIG_PATH . "> /dev/null &");
//        sleep(1);
//        restoreWorkWechatConfig();
//        restoreCookie($allCookie);
    } else {
        $resultMsg = "京东资产变动通知强化版\n未找到用户：$userid";
    }
    return $resultMsg;
}

function priceProtection($userid)
{
    $allCookie = '';
    // 替换ck
    $replaceRes = replaceCookie($userid, $allCookie);
    if ($replaceRes) {
        $resultMsg = "京东价格保护\n用户：$userid \n正在查询价格变动...\n稍后发送价保结果...\n";
        // 替换企业微信配置
        updateWorkWechatConfig($userid);
        exec("bash /jd/jd.sh jd_work_price now " . TMP_CONFIG_PATH . " > /dev/null &");
//        sleep(1);
//        恢复企业微信配置
//        restoreWorkWechatConfig();
//        恢复ck
//        restoreCookie($allCookie);
    } else {
        $resultMsg = "京东价格保护\n未找到用户：$userid";
    }
    return $resultMsg;
}

/**
 * beanSign 京豆签到
 * @param $userid
 * 2021/9/6 4:00 下午
 * @return string
 * @author 田继业 <tjy_we@163.com>
 */
function beanSign($userid)
{
    $allCookie = '';
    // 替换ck
    $replaceRes = replaceCookie($userid, $allCookie);
    if ($replaceRes) {
        $resultMsg = "京东多合一签到\n用户：$userid \n正在签到...\n稍后发送签到结果...\n";
        // 替换企业微信配置
        updateWorkWechatConfig($userid);
        exec("bash /jd/jd.sh jd_bean_sign now " . TMP_CONFIG_PATH . " > /dev/null &");
//        sleep(1);
//        恢复企业微信配置
//        restoreWorkWechatConfig();
//        恢复ck
//        restoreCookie($allCookie);
    } else {
        $resultMsg = "京东多合一签到\n未找到用户：$userid";
    }
    return $resultMsg;
}

/**
 * restoreCookie 恢复cookie
 * @param $allCookie
 * 2021/9/6 11:42 上午
 * @author 田继业 <tjy_we@163.com>
 */
function restoreCookie($allCookie)
{
    return;
    $config_sh              = file_get_contents(CONFIG_PATH);
    $cookies_content_arr    = explode("####cookie####\n", $config_sh);
    $cookies_content_arr[1] = $allCookie;
    file_put_contents(CONFIG_PATH, implode("####cookie####\n", $cookies_content_arr));
}

/**
 * replaceCookie 替换Cookie
 * @param $userid string 用户id
 * @param $allCookie string 保存所有的ck
 * 2021/9/6 11:41 上午
 * @return bool
 * @author 田继业 <tjy_we@163.com>
 */
function replaceCookie($userid, &$allCookie)
{

    $users = objToArray(json_decode(file_get_contents('./JDAccount.json')));

    if (empty($users[$userid])) {
        return false;
    }

    $config_sh           = file_get_contents(CONFIG_PATH);
    $cookies_content_arr = explode("####cookie####\n", $config_sh);
    $cookies             = $cookies_content_arr[1];
    $allCookie           = $cookies;
    $ck_arr              = explode("\n", $cookies);

    $updateCk = [];
    foreach ($ck_arr as $v) {
        $ck = explode("=\"", $v);
        if ($ck)
            if (!empty($ck[0]) && !empty($ck[1])) {
                $ck        = substr($ck[1], 0, -1);
                $pt_pin    = explode(";", $ck)[1];
                $user_name = explode('=', $pt_pin)[1];
                $user_name = urldecode($user_name);
                if (in_array($user_name, $users[$userid])) {
                    $updateCk[] = $ck;
                }
            }
    }
    $content = '';
    foreach ($updateCk as $k => $v) {
        $content .= 'Cookie' . ($k + 1) . '=' . '"' . $v . '"' . PHP_EOL;
    }
    if ($content) {
        $cookies_content_arr[1] = $content;
        file_put_contents(TMP_CONFIG_PATH, implode("####cookie####\n", $cookies_content_arr));
        return true;
    }

    return false;
}

/**
 * getCookies 微信用户获取ck
 * @param $userid
 * 2021/9/10 10:26 上午
 * @return array|false
 * @author 田继业 <tjy_we@163.com>
 */
function getCookies($userid)
{
    $users = objToArray(json_decode(file_get_contents('./JDAccount.json')));

    if (empty($users[$userid])) {
        return false;
    }

    $config_sh           = file_get_contents(CONFIG_PATH);
    $cookies_content_arr = explode("####cookie####\n", $config_sh);
    $cookies             = $cookies_content_arr[1];
    $ck_arr              = explode("\n", $cookies);

    $updateCk = [];
    foreach ($ck_arr as $v) {
        if ($ck = explode("=\"", $v))
            if (!empty($ck[0]) && !empty($ck[1])) {
                $ck        = substr($ck[1], 0, -1);
                $pt_pin    = explode(";", $ck)[1];
                $user_name = explode('=', $pt_pin)[1];
                $user_name = urldecode($user_name);
                if (in_array($user_name, $users[$userid])) {
                    $updateCk[] = $ck;
                }
            }
    }
    return $updateCk;
}


/**
 * restoreWorkWechatConfig 恢复微信通知配置
 * 2021/9/6 11:42 上午
 * @author 田继业 <tjy_we@163.com>
 */
function restoreWorkWechatConfig()
{
    return;
    $config_sh     = file_get_contents(CONFIG_PATH);
    $config_arr    = explode("####WorkWechat####\n", $config_sh);
    $config_arr[1] = 'export QYWX_AM="' . WX_CORPID . ',' . WX_CORPSECRET . ',' . '@all' . ',' . WX_AGENT_ID . ',' . WX_THUMB_MEDIA_ID . '"' . PHP_EOL;
    file_put_contents(CONFIG_PATH, implode("####WorkWechat####\n", $config_arr));
}

/**
 * updateMenus 更新应用菜单
 * @param $app
 * 2021/9/6 11:42 上午
 * @return mixed
 * @author 田继业 <tjy_we@163.com>
 */
function updateMenus($app)
{
    $menus = [

        'button' => [
            [
                'name'       => '资产通知',
                'sub_button' => [
                    [
                        'name' => '普通版',
                        'type' => 'click',
                        'key'  => '资产变动通知'
                    ],
                    [
                        'name' => '强化版',
                        'type' => 'click',
                        'key'  => '资产变动通知强化版'
                    ],
                ]
            ],
            // [
            //     'name'       => '评价&价保',
            //     'sub_button' => [
            //         [
            //             'name' => '自动评价',
            //             'type' => 'click',
            //             'key'  => '自动评价'
            //         ],
            //         [
            //             'name' => '价格保护',
            //             'type' => 'click',
            //             'key'  => '价格保护'
            //         ]
            //     ]
            // ],
            [
                'name'       => '快捷操作',
                'sub_button' => [
                    // [
                    //     'name' => '登录',
                    //     'type' => 'click',
                    //     'key'  => '登录'
                    // ],
                    [
                        'name' => '查询账户',
                        'type' => 'click',
                        'key'  => '查询绑定京东账户'
                    ]
                ]
            ]
        ],
    ];

    return json_encode($app->menu->create($menus));
}

/**
 * commandHelp 获取帮助
 * @param $commands
 * 2021/9/6 4:00 下午
 * @return string
 * @author 田继业 <tjy_we@163.com>
 */
function commandHelp($commands)
{
    $msg = "查询已绑定的京东账户指令：\n\t查询绑定京东账户" . PHP_EOL . PHP_EOL;
    $msg .= "绑定京东账户指令：\n\t绑定京东账户 京东账户名,可绑定多个京东账户" . PHP_EOL . PHP_EOL;
    $msg .= "解绑京东账户指令：\n\t解绑京东账户 京东账户名,一次只能解绑一个" . PHP_EOL . PHP_EOL;
    $msg .= '可发送以下指令执行对应任务：' . PHP_EOL;
    foreach ($commands as $key => $value) {
        $msg .= $key . PHP_EOL;
    }
    return $msg;
}

/**
 * sendWxNotify 发送企业微信通知
 * @param $username
 * @param $content
 * @param $title string
 * 2021/9/2 3:56 下午
 * @author 田继业 <tjy_we@163.com>
 */
function sendHelpWxNotify($username, $content, $title = '命令帮助')
{
    $res          = request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/gettoken', json_encode(['corpid' => WX_CORPID, 'corpsecret' => WX_CORPSECRET]));
    $token_result = json_decode($res);
    if ($token_result->errcode == 0) {

        $access_token = $token_result->access_token;
        $html         = str_replace("\n", "<br>", $content);
        $options      = [
            'touser'  => $username,
            'agentid' => WX_AGENT_ID,
            'safe'    => 0,
            'msgtype' => 'mpnews',
            'mpnews'  => [
                'articles' => [
                    [
                        'title'              => $title,
                        'thumb_media_id'     => WX_THUMB_MEDIA_ID,
                        'author'             => '智能助手',
                        'content_source_url' => '',
                        'content'            => $html,
                        'digest'             => $content
                    ]
                ]
            ]
        ];
        request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . $access_token, json_encode($options));
    }
}

function sendWxMsgCurrent($username, $content)
{
    $res          = request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/gettoken', json_encode(['corpid' => WX_CORPID, 'corpsecret' => WX_CORPSECRET]));
    $token_result = json_decode($res);
    if ($token_result->errcode == 0) {
        $access_token = $token_result->access_token;
        $options      = [
            'touser'  => $username,
            'agentid' => WX_AGENT_ID,
            'safe'    => 0,
            'msgtype' => 'mpnews',
            'mpnews'  => [
                'articles' => [
                    [
                        'title'              => '用户cookie已更新',
                        'thumb_media_id'     => WX_THUMB_MEDIA_ID,
                        'author'             => '智能助手',
                        'content_source_url' => '',
                        'content'            => $content,
                        'digest'             => $username
                    ]
                ]
            ]
        ];
        request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . $access_token, json_encode($options));
    }
}

