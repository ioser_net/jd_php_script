<?php
require './functions.php';

if (empty(WX_CORPID)
    || empty(WX_AGENT_ID)
    || empty(WX_CORPSECRET)
    || empty(WX_SERVER_TOKEN)
    || empty(WX_SERVER_AES_KEY)
    || empty(WX_THUMB_MEDIA_ID)
    || empty(YOU_WX_USER_ID)) {
    echo "请检查配置文件是否填写正确";
    die;
}


// 判断是否首次运行
if (!file_exists('./init_flag')) {
    $config_sh = file_get_contents(CONFIG_PATH);
    if (!empty($config_sh)) {
        $config_sh = str_replace("Cookie1=\"\"\nCookie2=\"\"\nCookie3=\"\"\nCookie4=\"\"\nCookie5=\"\"\nCookie6=\"\"\n", "####cookie####\n####cookie####\n", $config_sh);
        $config_sh = str_replace("export QYWX_AM=\"\"\n", "####WorkWechat####\nexport QYWX_AM=\"" . implode(',', [WX_CORPID, WX_CORPSECRET, '@all', WX_AGENT_ID, WX_THUMB_MEDIA_ID]) . "\"\n####WorkWechat####\n", $config_sh,);
        $config_sh = str_replace("export DD_BOT_TOKEN=\"\"\nexport DD_BOT_SECRET=\"\"\n", "export DD_BOT_TOKEN=\"" . DD_TOKEN . "\"\nexport DD_BOT_SECRET=\"" . DD_SECRET . "\"\n", $config_sh);
        file_put_contents('./init_flag', 'ok');
        file_put_contents(CONFIG_PATH, $config_sh);
    }
}
$requestData = $_POST;
if (isPost()) {

    if (!empty($requestData['code']) & !empty($requestData['cookie'])) {
        $code       = $requestData['code'];
        $new_cookie = $requestData['cookie'];
        if (preg_match('/^pt_key=(.*?);pt_pin=(.*?);/', urldecode($new_cookie))) {
            $pt_key = explode(";", $new_cookie)[0];
            $key    = explode('=', $pt_key)[1];
//            if (strlen($key) !== 75 || strlen($key) !== 96) {
//                echo json_encode(['code' => -1, 'msg' => 'cookie格式错误']);
//                die;
//            }
        } else {
            echo json_encode(['code' => -1, 'msg' => 'cookie格式错误']);
            die;
        }

        if (!empty($requestData['time_zone'])) {
            date_default_timezone_set($requestData['time_zone']);
        }
        try {
            if ($code != date('YmdHi')) {
                echo json_encode(['code' => -1, 'msg' => '验证码错误']);
                die;
            }
            $config_sh           = file_get_contents(CONFIG_PATH);
            $cookies_content_arr = explode("####cookie####\n", $config_sh);
            $cookies             = $cookies_content_arr[1];
            $ck_arr              = explode("\n", $cookies);

            $pt_pin = explode(";", $new_cookie)[1];
            $flag   = 0;
            foreach ($ck_arr as $v) {
                if ($ck = explode("=\"", $v))
                    if (!empty($ck[0]) && !empty($ck[1])) {
                        $cks[$ck[0]] = substr($ck[1], 0, -1);
                        if ($pt_pin == explode(";", $cks[$ck[0]])[1] || $pt_pin == urldecode(explode(";", $cks[$ck[0]])[1])) {

                            $urlencode_ptpin = explode('=', $pt_pin)[1];
                            $urlencode_ck    = $pt_key . ";pt_pin=" . $urlencode_ptpin . ';';
//                            file_put_contents('./ptpin.txt', $urlencode_ck.PHP_EOL, FILE_APPEND);
                            $cks[$ck[0]] = $urlencode_ck;
                            $flag        = 1;
                        }
                    }
            }
            if ($flag == 0) {
                $urlencode_ptpin                   = explode('=', $pt_pin)[1];
                $urlencode_ck                      = $pt_key . ";pt_pin=" . $urlencode_ptpin . ';';
                $cks['Cookie' . (count($cks) + 1)] = $urlencode_ck;
            }
            $content = '';
            foreach ($cks as $k => $v) {
                $content .= $k . '=' . '"' . $v . '"' . PHP_EOL;
            }
            $cookies_content_arr[1] = $content;
            file_put_contents(CONFIG_PATH, implode("####cookie####\n", $cookies_content_arr));
            $user_name = explode('=', $pt_pin)[1];
            $user_name = urldecode($user_name);
            sendWxNotify($user_name);
            sendDDNotify($user_name);
        } catch (\Exception $exception) {
            echo json_encode(['code' => -1, 'msg' => 'cookie格式错误']);
            die;
        }
        echo json_encode(['code' => 1, 'msg' => 'success']);
        die;
    } else {
        echo json_encode(['code' => -1, 'msg' => '请补全数据']);
        die;
    }
}


/**
 * sendDDNotify 发送钉钉通知
 * @param $username
 * 2021/9/2 3:56 下午
 * @author 田继业 <tjy_we@163.com>
 */
function sendDDNotify($username)
{
    if (empty(DD_SECRET) || empty(DD_TOKEN)) return;
    $m    = "{\"msgtype\": \"markdown\",\"markdown\": {\"title\":\"京东自助提交Cookie\", \"text\": \"#### 京东自助提交Cookie  \n\n > 用户：$username \n\n >  已更新 \"},\"at\": {\"atMobiles\": [],\"isAtAll\": false }}";
    $time = time() . '000';
    $sign = utf8_encode(urlencode(base64_encode(hash_hmac('sha256', $time . "\n" . DD_SECRET, DD_SECRET, true))));
    request_by_curl(
        'https://oapi.dingtalk.com/robot/send?access_token=' . DD_TOKEN . '&timestamp=' . $time . '&sign=' . $sign,
        $m);
}

/**
 * sendWxNotify 发送企业微信通知
 * @param $username
 * 2021/9/2 3:56 下午
 * @author 田继业 <tjy_we@163.com>
 */
function sendWxNotify($username)
{
    $res          = request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/gettoken', json_encode(['corpid' => WX_CORPID, 'corpsecret' => WX_CORPSECRET]));
    $token_result = json_decode($res);
    if ($token_result->errcode == 0) {
        $access_token = $token_result->access_token;
        $options      = [
            'touser'  => '@all',
            'agentid' => WX_AGENT_ID,
            'safe'    => 0,
            'msgtype' => 'mpnews',
            'mpnews'  => [
                'articles' => [
                    [
                        'title'              => '用户cookie已更新',
                        'thumb_media_id'     => WX_THUMB_MEDIA_ID,
                        'author'             => '智能助手',
                        'content_source_url' => '',
                        'content'            => $username,
                        'digest'             => $username
                    ]
                ]
            ]
        ];
        request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . $access_token, json_encode($options));
    }
}

?>
<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <title>京东自助提交Cookie</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="UTF-8" name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta content="yes" name="apple-touch-fullscreen">
    <meta content="black" name="apple-mobile-web-app-status-bar-style">
    <meta content="telephone=no" name="format-detection">
    <link rel="stylesheet" type="text/css" href="http://serve.qhdyzb.cn/static/admin/css/web_style.css"/>
    <style>
        header {
            color: #fff;
            font-size: 0.45rem;
            height: 64px;
            line-height: 64px;
            background-color: rgb(43, 143, 198);
        }

        .content {
            text-align: left;
        }

        .input-box {
            width: 7rem;
            margin: 0.1rem auto;
        }

        .input-box input {
            border: 1px solid rgb(180, 180, 180);
            border-radius: 0.1rem;
            height: 0.7rem;
            width: 100%;
            display: block;
            margin: 0.1rem auto;
            padding-left: 0.1rem;
            font-size: 0.36rem;
            outline: none;
        }

        .input-box textarea {
            border: 1px solid rgb(180, 180, 180);
            border-radius: 0.1rem;
            height: 3rem;
            width: 100%;
            display: block;
            margin: 0.1rem auto;
            padding: 0.1rem;
            font-size: 0.36rem;
            outline: none;
        }

        button {
            margin: 0.4rem auto;
            display: block;
            width: 7rem;
            height: 1rem;
            line-height: 1rem;
            font-size: 0.4rem;
            color: white;
            border-radius: 0.1rem;
            background-color: rgb(43, 143, 198);
        }
    </style>
</head>
<body>
<header>京东自助提交Cookie</header>
<div class="content">
    <div class="input-box"><label>1.防止外人乱用，请输入验证码：</label>
        <div><input type="text" name="code" id="code"/></div>
    </div>
    <div class="input-box"><label>2.把工具生成的cookie粘贴到这儿。</label>
        <div><textarea name="cookie" id="cookie"></textarea></div>
    </div>
    <button type="button" onclick="submit()">提交</button>
</div>
<script src="http://serve.qhdyzb.cn/js/jquery.min.js?v=1"></script>
<script src="http://serve.qhdyzb.cn/js/rem.js?v=1"></script>
</body>
<script type="text/javascript">
    function submit() {
        let code = $('#code').val();
        let cookie = $('#cookie').val();


        $.ajax({
            url: './index.php',
            method: 'post',
            data: {
                code: code,
                cookie: cookie
            },
            success: function (res) {
                res = JSON.parse(res)
                if (res.code === 1) {
                    alert('更新成功')
                    // location.href = '{:url("success")}'
                } else {
                    alert(res.msg)
                }
            }
        })
    }
</script>

</html>

