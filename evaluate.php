<?php
ini_set('memory_limit', -1);
set_time_limit(0);
require_once './config.php';
require_once './functions.php';
require_once './vendor/autoload.php';

use EasyWeChat\Factory;
use EasyWeChat\Kernel\Messages\Image;
use GuzzleHttp\Client;

define('USER_AGENT', "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Mobile Safari/537.36");


if (empty(WX_CORPID) || empty(WX_AGENT_ID) || empty(WX_CORPSECRET) || empty(WX_SERVER_TOKEN) || empty(WX_SERVER_AES_KEY)) {
    echo "请检查配置文件是否填写正确";
    die;
}


/**
 * start 开始评价
 * @param $userId
 * @param $cookie_str
 * 2021/9/10 2:26 下午
 * @author 田继业 <tjy_we@163.com>
 */
function start($userId, $cookie_str)
{
    $cookies = explode(',', $cookie_str);
    if (is_array($cookies) && !empty($cookies)) {
        sendWxMsg($userId, "自动评价\n共检测到" . count($cookies) . "个cookie,开始获取用户信息");
        foreach ($cookies as $k => $cookie) {

            $user = getUserInfo($cookie);
            if (is_array($user)) {
                sendWxMsg($userId, "自动评价\n已经获取到第" . ($k + 1) . "个用户信息\n用户昵称：$user[nickname]");
                sendWxMsg($userId, "自动评价\n正在获取第" . ($k + 1) . "个用户订单。。。。。。\n很慢。。。。\n别着急。\n\n每次单用户最多获取10个商品订单，10个服务订单\n订单过多请分多次执行");
                $orderList = getEvalOrderList($userId, $cookie);
                if (!empty($orderList)) {
                    sendWxMsg($userId, "自动评价\n获取到第" . ($k + 1) . "个用户：\n用户昵称：$user[nickname]\n共有" . count($orderList) . "个订单");

                    $msg = "自动评价\n订单列表:\n";
                    foreach ($orderList as $index => $order) {
                        $orderType = $order['orderType'] == 'goods' ? '商品评价' : '服务评价';
                        $msg       .= "\t订单" . ($index + 1) . ":" . $order['goodsName'] . ", 评价类型：" . $orderType . "\n";
                    }
                    sendWxMsg($userId, $msg);
                    sendEvaluate($orderList, $cookie, $userId);
                    sendWxMsg($userId, "自动评价\n第" . ($k + 1) . "个用户信息\n用户昵称：$user[nickname]\n订单已经全部评价完成\n暂停5s开始评价下一用户订单");
                } else {
                    sendWxMsg($userId, "自动评价\n获取到第" . ($k + 1) . "个用户：\n用户昵称：$user[nickname]\n共有" . count($orderList) . "个订单,开始进行下一个用户");
                }

            } else {
                sendWxMsg($userId, "自动评价\n未获取到第" . ($k + 1) . "个用户信息\n 错误信息：$user");
            }
            sleep(5);
        }

    } else {
        sendWxMsg($userId, "自动评价\n未获取到有效cookie");
    }
    sendWxMsg($userId, "自动评价\n所有用户订单已经评价完成");
    //解锁用户 不让这个用户继续执行
    $currentUser          = objToArray(json_decode(file_get_contents('EvalCurrentUser.json')));
    $currentUser[$userId] = false;
    file_put_contents('EvalCurrentUser.json', json_encode($currentUser));
}

/**
 * getEvalOrderList 待评价列表
 * @param $cookie
 * 2021/9/10 10:34 上午
 * @return array
 * @author 田继业 <tjy_we@163.com>
 */
function getEvalOrderList($userId, $cookie)
{
    $orderList    = getOrderList($cookie);
    $waitEvalList = [];
    if ($orderList && is_array($orderList)) {
        foreach ($orderList as $order) {
            $orderType = 'service';
            foreach ($order['buttonList'] as $button) {
                if ($button['name'] == '评价晒单') {
                    $orderType = 'goods';
                }
            }
            $userCommentVoList = getEvelInfo($order['orderId'], $cookie);
            if ($userCommentVoList && is_array($userCommentVoList)) {
                foreach ($userCommentVoList as $goods) {
                    $evalContent = [
                        'goodsName' => $goods['productSolrInfo']['fullName'],
                        'productId' => $goods['productSolrInfo']['id'],
                        'orderId'   => $order['orderId'],
                        'orderType' => $orderType
                    ];
                    if ($goods['service'] == 1 && $orderType == 'goods') {
                        $waitEvalList[]           = $evalContent;
                        $evalContent['orderType'] = 'service';
                        $waitEvalList[]           = $evalContent;
                    } else if ($goods['service'] == 0) {
                        $waitEvalList[] = $evalContent;
                    } else {
                        $waitEvalList[] = $evalContent;
                    }
                }
            }
        }
        sleep(2);
    }

    foreach ($waitEvalList as &$eval) {
        if ($eval['orderType'] == 'goods') {
            $commentList = getCommentList($eval['productId'], $cookie);

            if ($commentList && is_array($commentList)) {
                foreach ($commentList as $comment) {
                    if (!empty($comment['images'])) {
                        $images = [];
                        foreach ($comment['images'] as $image) {
                            $url      = substr($image['imgUrl'], 5);
                            $url      = str_replace('s128x96_jfs', 'jfs', $url);
                            $images[] = $url;
                        }
                        $comment['imageJson'] = implode(',', $images);
                        $eval['comment']      = $comment;
                    }
                }
                if (empty($eval['comment']) && !empty($commentList)) {
                    $eval['comment'] = $commentList[0];
                }
            } else {
                sendWxMsg($userId, '获取评论失败，原因：' . json_encode($commentList));
            }
        }
        sleep(2);
    }
    return $waitEvalList;

}

/**
 * sendEvaluate 发送评价
 * @param $waitEvalList
 * @param $cookie
 * @param $userId
 * 2021/9/10 2:25 下午
 * @author 田继业 <tjy_we@163.com>
 */
function sendEvaluate($waitEvalList, $cookie, $userId)
{
    $waitEvalList = sort_multi_array_with_key($waitEvalList, 'orderType');
    foreach ($waitEvalList as $k => $eval) {
        if ($eval['orderType'] == 'goods') {
            if (!empty($eval['comment'])) {
                $content = $eval['comment']['content'] ?? '很好，非常好，下次还会买';
                $image   = $eval['comment']['imageJson'] ?? '';
                sendWxMsg($userId, "自动评价\n开始评价第" . ($k + 1) . "条订单\n" . $eval['goodsName'] . "\n评价类型：" . "商品评价\n\n评价内容：" . $content . PHP_EOL);

                if (!empty($image)) {
                    sendWxMsg($userId, "自动评价\n图片列表：。。。");
                    $images = explode(',', $image);
                    foreach ($images as $img) {
                        sendWxImg($userId, $img);
                    }
                }
                echo "评价图片：" . $eval['comment']['imageJson'] . PHP_EOL;
                $evalRes = sendEvel($eval['productId'], $eval['orderId'], $content, $image, $cookie);
            } else {
                $evalRes = sendEvel($eval['productId'], $eval['orderId'], '很好，非常好，下次还会买', '', $cookie);
            }
            if (!empty($evalRes) && $evalRes === true) {
                sendWxMsg($userId, "自动评价\n" . $eval['goodsName'] . "\n评价成功\n休息5s进行下一条");
            } else {

                sendWxMsg($userId, "自动评价\n" . $eval['goodsName'] . "\n评价失败\n失败原因：" . json_encode($evalRes) . "\n休息5s进行下一条");
            }

        } else {
            sendWxMsg($userId, "自动评价\n开始评价第" . ($k + 1) . "条订单\n" . $eval['goodsName'] . "\n评价类型：" . "服务评价");
            $evalRes = sendDSR($eval['orderId'], $cookie);
            if ($evalRes === true) {
                sendWxMsg($userId, "自动评价\n" . $eval['goodsName'] . "\n评价成功\n休息5s进行下一条");
            } else {
                sendWxMsg($userId, "自动评价\n" . $eval['goodsName'] . "\n评价失败\n失败原因：" . $evalRes . "\n休息5s进行下一条");
            }
        }
        sleep(5);
    }
}

/**
 * getEvelInfo 获取评价详情
 * @param $orderId
 * @param $cookie
 * 2021/9/10 9:17 上午
 * @return false|mixed
 * @author 田继业 <tjy_we@163.com>
 */
function getEvelInfo($orderId, $cookie)
{
    $time       = time();
    $uri        = "/eval/GetEvalPage?orderId=$orderId&operation=16&pageIndex=1&pageSize=100&_=$time&g_login_type=0&callback=&g_tk=&g_ty=ls";
    $referer    = "https://wqitem.jd.com/";
    $user_agent = "jdapp;iPhone;10.0.2;14.1;a40b052e9d71daa99c3dbdf6f9d8ab38d41d8cd9;network/wifi;ADID/0BC966C6-2DE3-D6EE-5637-5D954FE38ECE;model/iPhone9,2;addressid/7133115097;appBuild/167694;jdSupportDarkMode/0;Mozilla/5.0 (iPhone; CPU iPhone OS 14_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) baiduboxapp/249908 Mobile/15E148;supportJDSHWK/1";
    $host       = "wq.jd.com";
    $client     = new Client([
        // Base URI is used with relative requests
        'base_uri' => "https://$host",
        // You can set any number of default request options.
        'timeout'  => 5.0,
        'headers'  => [
            'User-Agent' => $user_agent,
            'Cookie'     => $cookie,
            'Referer'    => $referer,
            'Host'       => $host,
            'Accept'     => '*/*',
            'Connection' => 'keep-alive'
        ]
    ]);
    $result     = $client->request('GET', $uri)->getBody();
    $result     = objToArray(json_decode($result));

    if ($result['iRet'] == 13) {
        // cookie失效
        return false;
    }
    if ($result['iRet'] == 0) {
        $result = $result['data']['jingdong_club_voucherbyorderid_get_response'];
        if ($result['resultCode'] == 1) {
            return $result['userCommentVoList'];
//        ["productSolrInfo"] => array(4) {
//            ["categoryList"] => string(0) ""
//            ["fullName"] => string(68) "【128g*10袋】哈尔滨香其酱 东北大酱 旋盖装 5袋原味"
//            ["id"] => int(10023584474383)
//            ["imgUrl"] => string(68) "jfs/t1/162535/31/20304/143678/607f839aE5a8f393f/539f1a5e091b2209.jpg"
//        }
//             $userCommentVoList;
        }
    }

    return false;

}

/**
 * sendDSR 评价服务
 * @param $orderId
 * @param $cookie
 * 2021/9/10 9:16 上午
 * @return bool
 * @author 田继业 <tjy_we@163.com>
 */
function sendDSR($orderId, $cookie)
{
    $time       = time();
    $uri        = "/eval/SendDSR?pin=&userclient=29&orderId=$orderId&otype=0&DSR1=5&DSR2=5&DSR3=5&_=$time&g_login_type=0&callback=&g_ty=ls";
    $referer    = "https://wq.jd.com/eval/SendDSR?pin=&userclient=29&orderId=$orderId&otype=0&DSR1=5&DSR2=5&DSR3=5&_=$time&g_login_type=0&callback=&g_ty=ls";
    $user_agent = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Mobile Safari/537.36";
    $host       = "wq.jd.com";
    $client     = new Client([
        // Base URI is used with relative requests
        'base_uri' => "https://$host",
        // You can set any number of default request options.
        'timeout'  => 5.0,
        'headers'  => [
            'User-Agent' => $user_agent,
            'Cookie'     => $cookie,
            'Referer'    => $referer,
            'Host'       => $host,
            'Accept'     => '*/*',
            'Connection' => 'keep-alive'
        ]
    ]);
    $result     = $client->request('GET', $uri)->getBody();
    $result     = objToArray(json_decode($result));
    if ($result['iRet'] == 0) {
        // 未提交成功
        $result = $result['data']['jingdong_club_tradecomment_save_responce'];
        if ($result['resultCode'] == 1) {
            return true;
//            errMsg
        }
        return $result['errMsg'] ?? false;
    } else {
        return $result['errMsg'];
    }
}

/**
 * sendEvel 评价商品
 * @param $productId
 * @param $orderId
 * @param $content
 * @param $imageJson
 * @param $cookie
 * 2021/9/10 9:16 上午
 * @return bool
 * @author 田继业 <tjy_we@163.com>
 */
function sendEvel($productId, $orderId, $content, $imageJson, $cookie)
{

    $uri        = "/eval/SendEval?g_login_type=0&g_ty=ajax";
    $referer    = "https://wqs.jd.com/";
    $user_agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1";
    $host       = "wq.jd.com";
    $client     = new Client([
        // Base URI is used with relative requests
        'base_uri' => "https://$host",
        // You can set any number of default request options.
        'timeout'  => 5.0,
        'headers'  => [
            'User-Agent'   => $user_agent,
            'Cookie'       => $cookie,
            'Referer'      => 'https://wqs.jd.com/',
            'Host'         => $referer,
            'Accept'       => '*/*',
            'Connection'   => 'keep-alive',
            'Content-Type' => 'application/x-www-form-urlencoded'
        ]
    ]);
    $body       = [
        'productId'     => $productId,
        'orderId'       => $orderId,
        'score'         => 5,
        'content'       => $content,
        'commentTagStr' => 1,
        'userclient'    => 29,
        'imageJson'     => $imageJson,
        'anonymous'     => 0,
        'syncsg'        => 0,
        'scence'        => 101100000,
        'videoid'       => '',
        'URL'           => '',
    ];
    $result     = $client->request('POST', $uri, ['body' => http_build_query($body)])->getBody();

    $result = objToArray(json_decode($result));

//    if ($result['iRet'] == 13) {
//        // cookie失效
//        return false;
//    }

    if ($result['iRet'] == 0) {
        return true;
    } else {
        return $result['errMsg'];
    }

}


/**
 * getOrderList 获取订单列表
 * 2021/9/9 5:05 下午
 * @return false|mixed
 * @author 田继业 <tjy_we@163.com>
 */
function getOrderList($cookie)
{
    $host     = "wq.jd.com";
    $time     = time();
    $uri      = "/bases/orderlist/list?order_type=8&start_page=1&last_page=0&page_size=10&callersource=mainorder&t=$time&traceid=&sceneval=2&g_login_type=1&callback=&g_ty=ls";
    $referer  = "https://wq.jd.com/bases/orderlist/list?order_type=8&start_page=1&last_page=0&page_size=10&callersource=mainorder&t=$time&traceid=&sceneval=2&g_login_type=1&callback=&g_ty=ls";
    $client   = new Client([
        // Base URI is used with relative requests
        'base_uri' => "https://$host",
        // You can set any number of default request options.
        'timeout'  => 5.0,
        'headers'  => [
            'User-Agent' => USER_AGENT,
            'Cookie'     => $cookie,
            'Referer'    => $referer,
            'Host'       => $host,
            'Accept'     => '*/*',
            'Connection' => 'keep-alive'
        ]
    ]);
    $result   = $client->request('GET', $uri)->getBody();
    $orderRes = objToArray(json_decode($result));

    if ($orderRes['errCode'] == 13) {
        // cookie失效
        return false;
    }
    if ($orderRes['errCode'] == 0) {
        return $orderRes['orderList'];
    }
    return false;

}

/**
 * getCommentList 获取评论列表
 * @param $sku
 * @param $cookie
 * 2021/9/10 9:38 上午
 * @return false|mixed
 * @author 田继业 <tjy_we@163.com>
 */
function getCommentList($sku, $cookie)
{
    $uri        = "/commodity/comment/getcommentlist?callback=&version=v2&pagesize=5&score=4&sku=$sku&sorttype=5&page=1&t=&sceneval=2&skucomment=1";
    $referer    = "https://wqitem.jd.com/";
    $user_agent = "jdapp;iPhone;10.0.3;13.5;7110a39bf50f8d62015ea2d9361d82c2d41d8cd9;network/wifi;ADID/87330FB9-E1F7-4A5D-5779-09B5FFA9C958;model/iPhone9,2;addressid/1980743145;appBuild/167694;jdSupportDarkMode/0;Mozilla/5.0 (iPhone; CPU iPhone OS 13_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) baiduboxapp/152275 Mobile/15E148;supportJDSHWK/1";
    $host       = "wq.jd.com";
    $client     = new Client([
        // Base URI is used with relative requests
        'base_uri' => "https://$host",
        // You can set any number of default request options.
        'timeout'  => 2.0,
        'headers'  => [
            'User-Agent' => $user_agent,
            'Cookie'     => $cookie,
            'Referer'    => $referer,
            'Host'       => $host,
            'Accept'     => '*/*',
            'Connection' => 'Keep-Alive',
            'Content-Type' => 'application/json;charset=UTF-8'
        ]
    ]);
    $result     = $client->request('GET', $uri)->getBody();
    $jsonPRes = stripslashes(html_entity_decode($result));
    $jsonPRes = jsonp_decode($jsonPRes);
    $objResult = json_decode($jsonPRes,true);
    $arrayRes = objToArray($objResult);

    if (intval($arrayRes['errcode']) == 0) {
        // 未获取到用户数据
        $commentResult = $arrayRes['result'];
        if (intval($commentResult['productCommentSummary']['CommentCount']) > 0) {
            return $commentResult['comments'];
        }
    } else {
        return $arrayRes['errmsg'];
    }
    return false;
}

/**
 * jsonp_decode jsonp数据格式解析
 * @param       $jsonp
 * @param false $assoc
 * 2021/9/9 5:04 下午
 * @return mixed
 * @author 田继业 <tjy_we@163.com>
 */
function jsonp_decode($jsonp)
{
    $jsonp = trim($jsonp);
//    $json = substr($json, strlen('commentCB('));
//    $json = substr($json, 0, -1);
//    return $json;
    if ($jsonp[0] !== '[' && $jsonp[0] !== '{') {
        $jsonp = substr($jsonp, strpos($jsonp, '('));

    }
    return trim($jsonp, '()');
    return json_decode(trim($jsonp, '();'), true);

}

/**
 * getUserInfo 获取京东用户信息
 * 2021/9/9 5:04 下午
 * @return false|mixed
 * @author 田继业 <tjy_we@163.com>
 */
function getUserInfo($cookie)
{
    $host        = "me-api.jd.com";
    $time        = time();
    $userInfoURI = "/user_new/info/GetJDUserInfoUnion?orgFlag=JD_PinGou_New&callSource=mainorder&channel=4&isHomewhite=0&sceneval=2&_=$time&sceneval=2&g_login_type=1&callback=&g_ty=ls";
    $referer     = "https://home.m.jd.com/myJd/newhome.action?sceneval=2&ufc=&sceneval=2&ufc=&sceneval=2&ufc=&";
    $client      = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'https://' . $host,
        // You can set any number of default request options.
        'timeout'  => 2.0,
        'headers'  => [
            'User-Agent' => USER_AGENT,
            'Cookie'     => $cookie,
            'Referer'    => $referer,
            'Host'       => $host
        ]
    ]);
    $userInfoRes = $client->request('GET', $userInfoURI)->getBody();
    $userInfoRes = objToArray(json_decode($userInfoRes));

    if ($userInfoRes['retcode'] == 1001) {
        // cookie失效
        return 1001;
    }
    if ($userInfoRes['retcode'] == 0) {
        $userInfo = $userInfoRes['data']['userInfo']['baseInfo'];
        return $userInfo;
    }
    return $userInfoRes['msg'];
//    dump($userInfo);
}

function sendWxMsg($username, $content)
{
    echo $content . PHP_EOL . PHP_EOL;
    $res          = request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/gettoken', json_encode(['corpid' => WX_CORPID, 'corpsecret' => WX_CORPSECRET]));
    $token_result = json_decode($res);
    if ($token_result->errcode == 0) {

        $access_token = $token_result->access_token;
        $options      = [
            'touser'  => $username,
            'agentid' => WX_AGENT_ID,
            'safe'    => 0,
            'msgtype' => 'text',
            'text'    => [
                'content' => $content
            ],
        ];
        request_by_curl('https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . $access_token, json_encode($options));
    }
}

function uploadMedia($url)
{
    $path = downImg('http:' . $url, md5($url));

    $workWechatConfig = [
        'corp_id'       => WX_CORPID,
        'agent_id'      => WX_AGENT_ID,
        'secret'        => WX_CORPSECRET,
        'token'         => WX_SERVER_TOKEN,
        'aes_key'       => WX_SERVER_AES_KEY,
        'response_type' => 'array',
        'log'           => [
            'level' => 'debug',
            'file'  => __DIR__ . '/wechat.log',
        ],
    ];

    $app      = Factory::work($workWechatConfig);
    $response = $app->media->uploadImage($path);
    if ($response['errcode'] == 0) {
        return $response['media_id'];
    }
    sleep(1);
    return false;

}

function sendWxImg($userId, $url)
{
    $media_id = uploadMedia($url);
    if ($media_id == false) {
        return false;
    }
    $workWechatConfig = [
        'corp_id'       => WX_CORPID,
        'agent_id'      => WX_AGENT_ID,
        'secret'        => WX_CORPSECRET,
        'token'         => WX_SERVER_TOKEN,
        'aes_key'       => WX_SERVER_AES_KEY,
        'response_type' => 'array',
        'log'           => [
            'level' => 'debug',
            'file'  => __DIR__ . '/wechat.log',
        ],
    ];

    $app       = Factory::work($workWechatConfig);
    $messenger = $app->messenger;
    $messenger->ofAgent(WX_AGENT_ID);
    $message  = new Image($media_id);
    $response = $messenger->message($message)->toUser($userId)->send();

}
