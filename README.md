# jd_php_script

#### 介绍
创建config.php,配置以下内容

```
<?php
define('CONFIG_PATH', '../config.sh');// config.sh文件路径
define('DD_TOKEN', '');// 钉钉webhook access-token
define('DD_SECRET', '');// 钉钉webhook secret
define('WX_CORPID', '');// 企业微信id
define('WX_CORPSECRET', ''); // 企业微信secret
define('WX_THUMB_MEDIA_ID', ''); //企业微信文章媒体图片
define('WX_AGENT_ID', ''); //企业微信应用agent_id
define('WX_SERVER_TOKEN',''); //企业微信服务端token
define('WX_SERVER_AES_KEY',''); //企业微信服务端aes-key
define('YOU_WX_USER_ID',''); //你自己的企业微信用户id
```

