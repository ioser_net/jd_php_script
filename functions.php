<?php
date_default_timezone_set('Asia/Shanghai');
require_once './config.php';
define('MSG_TIP_TEXT', "\n\n使用帮助：\n查询已绑定的京东账户指令：\n\t查询绑定京东账户" . PHP_EOL . PHP_EOL . "绑定京东账户指令：\n\t绑定京东账户 京东账户名,可绑定多个京东账户" . PHP_EOL . PHP_EOL . "解绑京东账户指令：\n\t解绑京东账户 京东账户名,一次只能解绑一个" . PHP_EOL . PHP_EOL);
define('COMMANDS', []);

function request_by_curl($remote_server, $post_string)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $remote_server);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=utf-8'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
    // curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    // curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function objToArray($obj)
{
    return json_decode(json_encode($obj), true);
}

/**
 * isPost 判断是否post
 * 2021/9/2 4:09 下午
 * @return int
 * @author 田继业 <tjy_we@163.com>
 */
function isPost()
{
    return strtoupper($_SERVER['REQUEST_METHOD']) == 'POST';
}

function dump(...$vars)
{
    ob_start();
    var_dump(...$vars);

    $output = ob_get_clean();
    $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);

    if (PHP_SAPI == 'cli') {
        $output = PHP_EOL . $output . PHP_EOL;
    } else {
        if (!extension_loaded('xdebug')) {
            $output = htmlspecialchars($output, ENT_SUBSTITUTE);
        }
        $output = '<pre>' . $output . '</pre>';
    }

    echo $output;
    die;
}

function sort_multi_array_with_key($arrays, $sort_key, $sort_order = SORT_DESC, $sort_type = SORT_NUMERIC)
{
    if (is_array($arrays)) {
        foreach ($arrays as $array) {
            if (is_array($array)) {
                $key_arrays[] = $array[$sort_key];
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
    array_multisort($key_arrays, $sort_order, $sort_type, $arrays);
    return $arrays;
}

//远程路径，名称，文件后缀
function downImg($url, $rename)
{
    $ext       = pathinfo($url)['extension'];
    $file_path = 'images/';
    if (!is_dir($_SERVER['DOCUMENT_ROOT'] . $file_path)) {
        mkdir($_SERVER['DOCUMENT_ROOT'] . $file_path);
    }
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    // 使用中文文件名需要转码
    $fp = fopen($file_path . $rename . "." . $ext, 'w');
    fwrite($fp, $rawdata);
    fclose($fp);
    // 返回路径
    return $_SERVER['DOCUMENT_ROOT'] . $file_path . $rename . "." . $ext;
}

/**
 * replaceCookie 替换Cookie
 * @param $userid string 用户id
 * @param $tmpCookiePath string 临时路径
 * 2021/9/6 11:41 上午
 * @return bool
 * @author 田继业 <tjy_we@163.com>
 */
function replaceCookieInShell($userid, $tmpCookiePath = TMP_CONFIG_PATH, $configPath = CONFIG_PATH)
{

    $users    = objToArray(json_decode(file_get_contents('./JDAccount.json')));
    $ttltoken = objToArray(json_decode(file_get_contents('./ttlToken.json')));

    if (empty($users[$userid])) {
        return false;
    }

    $config_sh           = file_get_contents($configPath);
    $cookies_content_arr = explode("####cookie####\n", $config_sh);
    $cookies             = $cookies_content_arr[1];
    $allCookie           = $cookies;
    $ck_arr              = explode("\n", $cookies);

    $updateCk = [];
    foreach ($ck_arr as $v) {
        $ck = explode("=\"", $v);
        if ($ck)
            if (!empty($ck[0]) && !empty($ck[1])) {
                $ck        = substr($ck[1], 0, -1);
                $pt_pin    = explode(";", $ck)[1];
                $user_name = explode('=', $pt_pin)[1];
                $user_name = urldecode($user_name);
                if (in_array($user_name, $users[$userid]) || in_array(urldecode($user_name),$users[$userid])) {
                    $updateCk[] = $ck;
                }
            }
    }
    $content = '';
    foreach ($updateCk as $k => $v) {
        $content .= 'Cookie' . ($k + 1) . '=' . '"' . $v . '"' . PHP_EOL;
    }

    if ($content) {
        $cookies_content_arr[1] = $content;
        $config_sh              = implode("####cookie####\n", $cookies_content_arr);
        $config_arr             = explode("####ttlhd####\n", $config_sh);



        if ($ttltoken[$userid]) {

            $ttl = array_values($ttltoken[$userid]);
            $config_arr[1] = 'export ttlhd="' . implode("@", $ttl) . '"' . PHP_EOL;
            file_put_contents('./'.$userid.'.txt',$config_arr[1]);
        } else {
            $config_arr[1] = '' . PHP_EOL;
        }
        file_put_contents($tmpCookiePath, implode("####ttlhd####\n", $config_arr));
        return true;
    }

    return false;
}

function getUnBindUsers($tmpCookiePath = TMP_CONFIG_PATH, $configPath = CONFIG_PATH)
{

    $users = objToArray(json_decode(file_get_contents('./JDAccount.json')));

    $user_arr = [];
    foreach ($users as $u) {
        $user_arr = array_merge($user_arr, $u);
    }

    $config_sh           = file_get_contents($configPath);
    $cookies_content_arr = explode("####cookie####\n", $config_sh);
    $cookies             = $cookies_content_arr[1];
    $allCookie           = $cookies;
    $ck_arr              = explode("\n", $cookies);

    $updateCk = [];
    foreach ($ck_arr as $v) {
        $ck = explode("=\"", $v);
        if ($ck)
            if (!empty($ck[0]) && !empty($ck[1])) {
                $ck        = substr($ck[1], 0, -1);
                $pt_pin    = explode(";", $ck)[1];
                $user_name = explode('=', $pt_pin)[1];
                $user_name = urldecode($user_name);
                if (!in_array($user_name, $user_arr)) {
                    $updateCk[] = $ck;
                }
            }
    }
    $content = '';
    foreach ($updateCk as $k => $v) {
        $content .= 'Cookie' . ($k + 1) . '=' . '"' . $v . '"' . PHP_EOL;
    }
    if ($content) {
        $cookies_content_arr[1] = $content;
        file_put_contents($tmpCookiePath, implode("####cookie####\n", $cookies_content_arr));
        return true;
    }

    return false;
}

/**
 * updateWorkWechatConfig 替换微信通知的用户
 * @param $userid
 * 2021/9/6 11:42 上午
 * @author 田继业 <tjy_we@163.com>
 */
function updateWorkWechatConfig($userid = '@all', $tmpConfigPath = TMP_CONFIG_PATH)
{
    $config_sh  = file_get_contents($tmpConfigPath);
    $config_arr = explode("####WorkWechat####\n", $config_sh);
    if ($userid == '@all') {
        $config_arr[1] = 'export QYWX_AM=""' . PHP_EOL;
    } else {
        $config_arr[1] = 'export QYWX_AM="' . WX_CORPID . ',' . WX_CORPSECRET . ',' . $userid . ',' . WX_AGENT_ID . ',' . WX_THUMB_MEDIA_ID . '"' . PHP_EOL;
    }

    file_put_contents($tmpConfigPath, implode("####WorkWechat####\n", $config_arr));

}
function getphonetype($phone){
    $phone = trim($phone);
    $isChinaMobile = "/^134[0-8]\d{7}$|^(?:13[5-9]|147|15[0-27-9]|17[28]|18[2-478]|19[58]|)\d{8}$/"; //移动
    $isChinaUnion = "/^1440\d{7}$|^(?:10145|10646)\d{6}$|^(?:13[0-2]|14[56]|15[56]|166|17[56]|18[56])\d{8}$/"; //联通
    $isChinaTelcom = "/^(?:133|153|173|177|18[019]|19[0139])\d{8}$/"; //电信
    if(preg_match($isChinaMobile, $phone)){
        return true;
    }elseif(preg_match($isChinaUnion, $phone)){
        return true;
    }elseif(preg_match($isChinaTelcom, $phone)){
        return true;
    }else{
        return false;
    }
}
