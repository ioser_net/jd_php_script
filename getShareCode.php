<?php
require  'functions.php';
$jdCityCodes = file_exists('./JDCurrentShareCode.json') ? objToArray(json_decode(file_get_contents('./JDCurrentShareCode.json'))):[];
$cityCodes = [
    'RtGKze72F1ije9LLRoU1hQptEbIonWu-CyBIyaal_-6pl18IVQ',
    'RtGKz7_xF1-rfIvMF4Vm1vkBMbRI2rKTbRTbtSwO5XSMq9_DwQ',
    'RtGKj6XEA0n6Kt3JSJV6moHKXn_NHJkENZU2i7QCLCKafEkx'
];
$jdCityCodes    = !empty($jdCityCodes) ? $jdCityCodes :  $cityCodes;
$jdCityCodes = array_filter($jdCityCodes);
$fruitCodes     = [
    'code' => 200,
    'data' => [
        '3b38c24930404451ba7f009e43b55692',
        'f7bc9d7e769542c3943a61170cf73890',
        'f8699d178c95427fb898c4d96d20d303',
        '3d41986fbf8f462d80ccfec658e5ba33',
        '41f87cd15a464e0fba85b7f898943d39']
];
$petCodes       = [
    'code' => 200,
    'data' => [
        'MTE0MDkyMjIwMDAwMDAwNDkzNzQzMzE=',
        'MTAxODc2NTEzMTAwMDAwMDAwNTYwNTYwMQ==',
        'MTAxODcxOTI2NTAwMDAwMDAyNTQzOTIwMQ==',
        'MTEyNjE4NjQ2MDAwMDAwMDUyNjEzMzY',
        'MTE1NDY3NTMwMDAwMDAwNTI2MTI2Mjk='
    ]
];
$beanCodes      = [
    'code' => 200,
    'data' => [
        'mlrdw3aw26j3xn2rxsbrgrly6q5nhw5zzaotg2y',
        '4npkonnsy7xi2wiorb4kgny2krnul3o5pk47xry',
        'u72q4vdn3zes3t3wyah7mbneyip5ttxuynyntda',
        'o7eiltak46s2xaqgvd3hw6kudgjobhkk7b35kty',
        '4oupleiwuds2aejyvra2tanow2poghhdpgeyjpi'
    ]
];
$jxFactoryCodes = [
    '7r4qNHT3I3Q0whoBBXMgaw=='
];
$ddFactoryCodes = [
    'T0225KkcRRxLowbXJUulxaYMJwCjVWnYaS5kRrbA',
    'T0225KkcR01MowHfIhKilKZfdACjVWnYaS5kRrbA',
    'T0205KkcB1d5txeOdESny7ZDCjVWnYaS5kRrbA'
];
$blindBoxCodes  = [
    'T0225KkcRRxLowbXJUulxaYMJwCjVQmoaT5kRrbA',
    'T0225KkcR01MowHfIhKilKZfdACjVQmoaT5kRrbA',
    'T0205KkcGE9nkzCTYVyv8oBwCjVQmoaT5kRrbA',
    'T0205KkcB1d5txeOdESny7ZDCjVQmoaT5kRrbA'
];
$ddworldCodes   = [
    [
        'use'  => '账号1',
        'code' => 'tfsjmI4euxN3lhWWYkxFCIwDodxToev5n61fvA0cg8w',
        'num'  => 0
    ], [
        'use'  => '账号2',
        'code' => 'eyCtphNk8JYuS_siwdyc37VHLZK2Y9DmnWVUcMApq_A',
        'num'  => 0
    ], [
        'use'  => '账号3',
        'code' => 'uoL1JqX5ACUiklCi8_lB1EELaXe',
        'num'  => 0
    ]
];
$jxCfdCodes     = [
    'shareId' => [
        '31C82062CCD88A1DE01F4381DDE19D4D468D3E5033D026FFD0D5E9EDAAEC7E58',
        'C8F15A0D54BCB46DECB2B962560395DF8604477B68E355932E4E1424D9034B4C',
        '382E25BD11BDBC7ED32DFE04CE9990DD192491D8F131A19F4CFC82A5E96AC0FD'
    ]
];
$jd_sgmhCodes  = [
    'code' => 200,
    'data' => [
        'T0225KkcRRxLowbXJUulxaYMJwCjVQmoaT5kRrbA',
        'T0225KkcRRxLowbXJUulxaYMJwCjVQmoaT5kRrbA',
        'u72q4vdn3zes3t3wyah7mbneyip5ttxuynyntda',
        'o7eiltak46s2xaqgvd3hw6kudgjobhkk7b35kty',
        '4oupleiwuds2aejyvra2tanow2poghhdpgeyjpi'
    ]
];

$codes          = [];
$type           = $_GET['type'];
switch ($type) {
    case 'JD_Fruit':
        $codes = $fruitCodes;
        break;
    case 'JD_Pet':
        $codes = $petCodes;
        break;
    case 'JD_Plant_Bean':
        $codes = $beanCodes;
        break;
    case 'JD_Factory':
        $codes = $ddFactoryCodes;
        break;
    case 'jd_CityNew':
        $codes = $jdCityCodes;
        break;
    case 'jd_cfd':
        $codes = $jxCfdCodes;
        break;
    case 'ddworld':
        $codes = $ddworldCodes;
        break;
    case 'jd_5g':
        $codes = $blindBoxCodes;
        break;
}
//$result = $codes[array_rand($codes)];
echo json_encode($codes);
die();
